package com.balinasoft.catalog.ui.base;

import android.support.v4.app.FragmentActivity;

/**
 * Created by kirill on 16.1.17.
 */

public interface BaseFragmentView extends BaseContextView {

    FragmentActivity getActivity();
}
