package com.balinasoft.catalog.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.balinasoft.catalog.R;
import com.balinasoft.catalog.database.model.Category;
import com.balinasoft.catalog.ui.activities.MainActivity;
import com.balinasoft.catalog.ui.adapter.CategoriesAdapter;
import com.balinasoft.catalog.ui.base.BaseFragment;
import com.balinasoft.catalog.ui.base.BasePresenter;
import com.balinasoft.catalog.ui.presenter.CategoriesFragmentPresenter;
import com.balinasoft.catalog.ui.view.CategoriesFragmentView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kirill on 16.1.17.
 */

public class CategoriesFragment extends BaseFragment implements CategoriesFragmentView {

    private static final int LAYOUT = R.layout.fragment_categories;

    private CategoriesFragmentPresenter mPresenter;

    @BindView(R.id.category_list_recycler)
    RecyclerView mCategoriesRec;
    CategoriesAdapter mAdapter;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    public static CategoriesFragment getInstance() {
        CategoriesFragment fragment = new CategoriesFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(LAYOUT, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MainActivity) getActivity()).setScreenTitle(getContext().getResources().getString(R.string.catalog));
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.loadCategory();
    }

    @Override
    public BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void createPresenter() {
        mPresenter = new CategoriesFragmentPresenter(this);
    }

    @Override
    public void setAdapter(List<Category> categories) {
        mAdapter = new CategoriesAdapter(getActivity(), categories);
        mCategoriesRec.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mCategoriesRec.setAdapter(mAdapter);
        mCategoriesRec.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void setProgressBarVis(boolean vis) {
        mCategoriesRec.setVisibility(vis ? View.GONE : View.VISIBLE);
        mProgressBar.setVisibility(vis ? View.VISIBLE : View.GONE);
    }
}
