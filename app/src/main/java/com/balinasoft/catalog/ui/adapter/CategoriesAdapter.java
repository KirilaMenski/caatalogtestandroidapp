package com.balinasoft.catalog.ui.adapter;

import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balinasoft.catalog.R;
import com.balinasoft.catalog.database.model.Category;
import com.balinasoft.catalog.ui.fragments.OffersFragment;
import com.balinasoft.catalog.util.FragmentUtils;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kirill on 16.1.17.
 */
public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesHolder> {

    private static final int LAYOUT = R.layout.item_category;

    private String[] categoryPictures = {"http://ufa.farfor.ru/media/menu/products/7.jpg",
            "http://ufa.farfor.ru/media/menu/products/%D0%91%D0%B0%D1%80%D0%B1%D0%B5%D0%BA%D1%8E_%D0%BF%D0%BE-%D0%B1%D0%BE%D0%BB%D0%B3%D0%B0%D1%80%D1%81%D0%BA%D0%B8_09_4.png",
            "http://ufa.farfor.ru/media/menu/products/%D1%83%D0%BD%D0%B0%D0%B3%D0%B8_%D0%BC%D0%B8%D0%BA%D1%81_%D1%81%D0%B0%D0%B9%D1%82.jpg",
            "http://ufa.farfor.ru/media/menu/products/%D1%85%D1%80%D1%83%D1%81%D1%82_.jpg",
            "http://ufa.farfor.ru/media/menu/products/%D0%A1%D1%83%D1%81%D0%B8_%D1%81_%D0%BA%D0%BE%D0%BF%D1%87%D0%B5%D0%BD%D1%8B%D0%BC_%D0%BB%D0%BE%D1%81%D0%BE%D1%81%D0%B5%D0%BC.jpg",
            "http://ufa.farfor.ru/media/menu/products/8022bca0845d9f9c6f9f820bd3d082ad.jpg",
            "http://ufa.farfor.ru/media/menu/products/IMG_0066.jpg", "http://ufa.farfor.ru/media/menu/products/%D0%BA%D0%BE%D0%BB%D0%B0.jpg",
            "http://ufa.farfor.ru/media/menu/products/311_%D0%97%D0%B0%D0%BA%D1%83%D1%81%D0%BA%D0%B8_%D0%9A%D0%B0%D1%80%D1%82%D0%BE%D1%84%D0%B5%D0%BB%D1%8C_%D0%A4%D1%80%D0%B8.jpg",
            "http://ufa.farfor.ru/media/menu/products/102_%D0%9B%D0%B0%D0%BF%D1%88%D0%B0_%D0%A1%D0%BE%D0%B1%D0%B0_%D1%81_%D0%BA%D1%83%D1%80%D0%B8%D1%86%D0%B5%D0%B8_%D0%B2_%D0%BE%D1%81%D1%82%D1%80%D0%BE-%D1%81%D0%BB%D0%B0%D0%B4%D0%BA%D0%BE%D0%BC_%D1%81%D0%BE%D1%83%D1%81%D0%B5.jpg",
            "http://ufa.farfor.ru/media/menu/products/344_%D0%A1%D0%B0%D0%BB%D0%B0%D1%82%D1%8B_%D0%A1%D0%B0%D0%BB%D0%B0%D1%82_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8C.jpg",
            "http://ufa.farfor.ru/media/menu/products/%D0%BC%D0%B8%D1%81%D0%BE_%D1%81%D1%8F%D0%BA%D0%B5.jpg",
            "http://ufa.farfor.ru/media/menu/products/%D1%82%D1%8F%D1%85%D0%BE%D0%BD-%D1%81-%D0%BA%D1%83%D1%80%D0%B8%D1%86%D0%B5%D0%B9.jpg"};
    private List<Category> mCategories;
    private WeakReference<FragmentActivity> mFragmentActivity;

    public CategoriesAdapter(FragmentActivity fragmentActivity, List<Category> categories) {
        mFragmentActivity = new WeakReference<>(fragmentActivity);
        mCategories = categories;
    }

    @Override
    public CategoriesAdapter.CategoriesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mFragmentActivity.get());
        View view = inflater.inflate(LAYOUT, parent, false);
        return new CategoriesHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoriesAdapter.CategoriesHolder holder, int position) {
        final Category category = mCategories.get(position);
        String picture = categoryPictures[position];
        holder.bindHolder(category, picture);
        holder.mCategoryCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentUtils.replaceFragment(mFragmentActivity.get(), R.id.main_fragment_container, OffersFragment.getInstance(category), true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class CategoriesHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_cover)
        ImageView mCategoryCover;
        @BindView(R.id.category_name)
        TextView mCategoryName;

        public CategoriesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindHolder(Category category, String picture) {
            setCover(picture);
            mCategoryName.setText(category.getName());
        }

        private void setCover(String picture) {
            Uri uri = Uri.parse(picture);
            Picasso.with(mFragmentActivity.get())
                    .load(uri)
                    .fit()
                    .centerCrop()
                    .into(mCategoryCover);
        }
    }

}
