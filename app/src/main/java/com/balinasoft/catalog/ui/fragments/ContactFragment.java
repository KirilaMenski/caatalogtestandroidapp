package com.balinasoft.catalog.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balinasoft.catalog.R;
import com.balinasoft.catalog.ui.activities.MainActivity;
import com.balinasoft.catalog.ui.base.BaseFragment;
import com.balinasoft.catalog.ui.base.BasePresenter;
import com.balinasoft.catalog.ui.presenter.ContactFragmentPresenter;
import com.balinasoft.catalog.ui.view.ContactFragmentView;

import butterknife.ButterKnife;

/**
 * Created by kirill on 16.1.17.
 */

public class ContactFragment extends BaseFragment implements ContactFragmentView {

    private static final int LAYOUT = R.layout.fragment_contact;

    private ContactFragmentPresenter mPresenter;

    public static ContactFragment getInstance() {
        ContactFragment fragment = new ContactFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(LAYOUT, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MainActivity) getActivity()).setScreenTitle(getContext().getResources().getString(R.string.contact));
    }

    @Override
    public BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void createPresenter() {
        mPresenter = new ContactFragmentPresenter(this);
    }


}
