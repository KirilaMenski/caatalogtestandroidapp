package com.balinasoft.catalog.ui.view;

import com.balinasoft.catalog.ui.base.BaseContextView;

/**
 * Created by kirill on 16.1.17.
 */
public interface OfferDetailsFragmentView extends BaseContextView {

    void setOfferCover(String coverPath);

    void setScreenTitle(String title);

    void setOfferName(String name);

    void setOfferWeight(double weight);

    void setOfferPrice(double price);

    void setOfferDescription(String description);

}
