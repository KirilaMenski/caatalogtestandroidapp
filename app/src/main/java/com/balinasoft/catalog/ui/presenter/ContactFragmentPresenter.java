package com.balinasoft.catalog.ui.presenter;

import com.balinasoft.catalog.ui.base.BaseContextView;
import com.balinasoft.catalog.ui.base.BasePresenter;
import com.balinasoft.catalog.ui.view.ContactFragmentView;

/**
 * Created by kirill on 16.1.17.
 */
public class ContactFragmentPresenter extends BasePresenter{

    private ContactFragmentView mView;

    public ContactFragmentPresenter(ContactFragmentView view){
        super(view.getContext());
        mView = view;
    }

    @Override
    public BaseContextView getView() {
        return mView;
    }
}
