package com.balinasoft.catalog.ui.presenter;

import android.os.Bundle;

import com.balinasoft.catalog.ui.base.BaseContextView;
import com.balinasoft.catalog.ui.base.BasePresenter;
import com.balinasoft.catalog.ui.view.MapFragmentView;

/**
 * Created by kirill on 18.1.17.
 */
public class MapFragmentPresenter extends BasePresenter{

    private MapFragmentView mView;

    public MapFragmentPresenter(MapFragmentView view){
        super(view.getContext());
        mView = view;
    }

    @Override
    public BaseContextView getView() {
        return mView;
    }

}
