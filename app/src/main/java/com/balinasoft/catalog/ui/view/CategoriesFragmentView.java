package com.balinasoft.catalog.ui.view;

import com.balinasoft.catalog.database.model.Category;
import com.balinasoft.catalog.ui.base.BaseContextView;

import java.util.List;

/**
 * Created by kirill on 16.1.17.
 */
public interface CategoriesFragmentView extends BaseContextView {

    void setAdapter(List<Category> categories);

    void setProgressBarVis(boolean vis);

}
