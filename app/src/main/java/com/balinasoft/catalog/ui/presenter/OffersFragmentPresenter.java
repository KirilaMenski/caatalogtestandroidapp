package com.balinasoft.catalog.ui.presenter;

import com.balinasoft.catalog.database.dao.CategoryDao;
import com.balinasoft.catalog.database.dao.OfferDao;
import com.balinasoft.catalog.database.daoimpl.CategoryDaoImpl;
import com.balinasoft.catalog.database.daoimpl.OfferDaoImpl;
import com.balinasoft.catalog.database.model.Category;
import com.balinasoft.catalog.database.model.Offer;
import com.balinasoft.catalog.ui.base.BaseContextView;
import com.balinasoft.catalog.ui.base.BasePresenter;
import com.balinasoft.catalog.ui.view.OffersFragmentView;

import java.util.List;

/**
 * Created by kirill on 16.1.17.
 */
public class OffersFragmentPresenter extends BasePresenter {

    OffersFragmentView mView;
    OfferDao mOfferDao;
    CategoryDao mCategoryDao;
    Category mCategory;

    public OffersFragmentPresenter(OffersFragmentView view) {
        super(view.getContext());
        mView = view;
        mOfferDao = OfferDaoImpl.getInstance();
        mCategoryDao = CategoryDaoImpl.getInstance();
    }

    public void initialize(Category category) {
        mCategory = category;
        mView.setScreenTitle(category.getName());
    }

    public void loadOffers() {
        List<Offer> offers = mCategory.getOffers();
        mView.setAdapter(offers);
    }

    @Override
    public BaseContextView getView() {
        return mView;
    }

}
