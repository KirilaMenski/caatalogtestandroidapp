package com.balinasoft.catalog.ui.presenter;

import com.balinasoft.catalog.database.model.Offer;
import com.balinasoft.catalog.ui.base.BaseContextView;
import com.balinasoft.catalog.ui.base.BasePresenter;
import com.balinasoft.catalog.ui.view.OfferDetailsFragmentView;

/**
 * Created by kirill on 16.1.17.
 */
public class OffersDetailsFragmentPresenter extends BasePresenter {

    private OfferDetailsFragmentView mView;
    private Offer mOffer;

    public OffersDetailsFragmentPresenter(OfferDetailsFragmentView view) {
        super(view.getContext());
        mView = view;
    }

    public void initializeViews(Offer offer) {
        mOffer = offer;
        mView.setScreenTitle(offer.getName());
        mView.setOfferCover(offer.getPicture());
        mView.setOfferName(offer.getName());
        mView.setOfferDescription(offer.getDescription());
        mView.setOfferWeight(offer.getWeight());
        mView.setOfferPrice(offer.getPrice());
    }

    @Override
    public BaseContextView getView() {
        return mView;
    }


}
