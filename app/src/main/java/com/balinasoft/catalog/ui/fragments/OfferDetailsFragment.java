package com.balinasoft.catalog.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balinasoft.catalog.R;
import com.balinasoft.catalog.database.model.Offer;
import com.balinasoft.catalog.ui.activities.MainActivity;
import com.balinasoft.catalog.ui.base.BaseFragment;
import com.balinasoft.catalog.ui.base.BasePresenter;
import com.balinasoft.catalog.ui.presenter.OffersDetailsFragmentPresenter;
import com.balinasoft.catalog.ui.view.OfferDetailsFragmentView;
import com.balinasoft.catalog.util.FragmentUtils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kirill on 16.1.17.
 */

public class OfferDetailsFragment extends BaseFragment implements OfferDetailsFragmentView {

    private static final int LAYOUT = R.layout.fragment_detail_offer;
    private static final String EXTRA_OFFER = "com.balinasoft.catalog.ui.fragments.offer";

    private OffersDetailsFragmentPresenter mPresenter;

    @BindView(R.id.offer_cover)
    ImageView mOfferCover;
    @BindView(R.id.offer_name)
    TextView mOfferName;
    @BindView(R.id.offer_weight)
    TextView mOfferWeight;
    @BindView(R.id.offer_price)
    TextView mOfferPrice;
    @BindView(R.id.offer_description)
    TextView mOfferDescription;

    public static OfferDetailsFragment getInstance(Offer offer) {
        OfferDetailsFragment fragment = new OfferDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_OFFER, offer);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(LAYOUT, container, false);
        ButterKnife.bind(this, view);
        Offer offer = (Offer) getArguments().getSerializable(EXTRA_OFFER);
        mPresenter.initializeViews(offer);
        return view;
    }

    @Override
    public BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onStart() {
        super.onStart();
        FragmentUtils.replaceFragment(getActivity(), R.id.map_container, MapFragment.newInstance(), false);
    }

    @Override
    protected void createPresenter() {
        mPresenter = new OffersDetailsFragmentPresenter(this);
    }

    @Override
    public void setOfferCover(String coverPath) {
        Picasso picassoInstance = Picasso.with(getContext());
        picassoInstance.load(coverPath)
                .fit()
                .centerCrop()
                .into(mOfferCover);
    }

    @Override
    public void setScreenTitle(String title) {
        ((MainActivity) getActivity()).setScreenTitle(title);
    }

    @Override
    public void setOfferName(String name) {
        mOfferName.setText(name);
    }

    @Override
    public void setOfferWeight(double weight) {
        mOfferWeight.setText(getContext().getResources().getString(R.string.offer_weight) + weight);
    }

    @Override
    public void setOfferPrice(double price) {
        mOfferPrice.setText(getContext().getResources().getString(R.string.offer_price) + price);
    }

    @Override
    public void setOfferDescription(String description) {
        mOfferDescription.setText(description);
    }
}
