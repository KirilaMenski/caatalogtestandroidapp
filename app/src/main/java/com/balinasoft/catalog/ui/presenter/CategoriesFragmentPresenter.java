package com.balinasoft.catalog.ui.presenter;

import android.util.Log;
import android.widget.Toast;

import com.balinasoft.catalog.R;
import com.balinasoft.catalog.api.ApiRequest;
import com.balinasoft.catalog.database.dao.CategoryDao;
import com.balinasoft.catalog.database.dao.OfferDao;
import com.balinasoft.catalog.database.daoimpl.CategoryDaoImpl;
import com.balinasoft.catalog.database.daoimpl.OfferDaoImpl;
import com.balinasoft.catalog.api.responsemodel.Catalog;
import com.balinasoft.catalog.database.model.Category;
import com.balinasoft.catalog.api.responsemodel.CategoryList;
import com.balinasoft.catalog.database.model.Offer;
import com.balinasoft.catalog.api.responsemodel.OfferResponse;
import com.balinasoft.catalog.ui.base.BaseContextView;
import com.balinasoft.catalog.ui.base.BasePresenter;
import com.balinasoft.catalog.ui.view.CategoriesFragmentView;
import com.balinasoft.catalog.util.NetworkUtils;

import java.util.List;

import rx.Observable;
import rx.Observer;


/**
 * Created by kirill on 16.1.17.
 */
public class CategoriesFragmentPresenter extends BasePresenter {

    private CategoryDao mCategoryDao;
    private OfferDao mOfferDao;

    private String[] mCategories = {"Патимейкер",
            "Пицца",
            "Сеты",
            "Роллы",
            "Суши",
            "Добавки",
            "Десерты",
            "Напитки",
            "Закуски",
            "Лапша",
            "Салаты",
            "Супы",
            "Теплое"};

    private static final String TAG = CategoriesFragmentPresenter.class.getSimpleName();
    CategoriesFragmentView mView;

    public CategoriesFragmentPresenter(CategoriesFragmentView view) {
        super(view.getContext());
        mView = view;
        mCategoryDao = CategoryDaoImpl.getInstance();
        mOfferDao = OfferDaoImpl.getInstance();
    }

    public void loadCategory() {
        if (mCategoryDao.getCategories() == null || mCategoryDao.getCategories().size() == 0) {
            loadFromXml();
        } else {
            loadFromDataBase();
        }
    }

    private void loadFromXml() {
        if (NetworkUtils.isNetworkConnected(getContext())) {
            mView.setProgressBarVis(true);
            Observable<Catalog> observable = ApiRequest.getInstance().getApi().loadData();
            Observer<Catalog> observer = new Observer<Catalog>() {
                @Override
                public void onCompleted() {
                    mView.setProgressBarVis(false);
                    loadFromDataBase();
                }

                @Override
                public void onError(Throwable e) {
                    mView.setProgressBarVis(false);
                    Log.i(TAG, "Error", e);
                }

                @Override
                public void onNext(Catalog catalog) {
                    List<CategoryList> lists = catalog.getShop().getCategoryResponses().getCategoryLists();
                    List<OfferResponse> offerResponses = catalog.getShop().getOffer().getOffers();
                    for (int i = 0; i < lists.size(); i++) {
                        Category category = new Category();
                        category.setCategoryId(lists.get(i).getId());
                        category.setName(mCategories[i]);
                        Log.i(TAG, "id: " + lists.get(i).getCategory());
                        mCategoryDao.addCategory(category);
                    }

                    for (int i = 0; i < offerResponses.size(); i++) {
                        Category category = mCategoryDao.getCategoryById(offerResponses.get(i).getCategoryId());
                        Offer offer = new Offer();
                        offer.setOfferId(offerResponses.get(i).getId());
                        offer.setName(offerResponses.get(i).getName());
                        offer.setCategory(category);
                        offer.setDescription(offerResponses.get(i).getDescription());
                        offer.setPrice(offerResponses.get(i).getPrice());
                        offer.setPicture(offerResponses.get(i).getPicture());
                        offer.setUrl(offerResponses.get(i).getUrl());
                        mOfferDao.addOffer(offer);
                    }

                }
            };
            bindObservable(observable, observer);
        } else {
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.failed_to_connect), Toast.LENGTH_LONG).show();
        }
    }

    private void loadFromDataBase() {
        List<Category> categories = mCategoryDao.getCategories();
        mView.setAdapter(categories);
    }

    @Override
    public BaseContextView getView() {
        return mView;
    }
}
