package com.balinasoft.catalog.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.balinasoft.catalog.R;
import com.balinasoft.catalog.ui.base.BaseActivity;
import com.balinasoft.catalog.ui.base.BasePresenter;
import com.balinasoft.catalog.ui.fragments.CategoriesFragment;
import com.balinasoft.catalog.ui.fragments.ContactFragment;
import com.balinasoft.catalog.util.FragmentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements FragmentManager.OnBackStackChangedListener {

    private static final int LAYOUT = R.layout.activity_main;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.tv_screen_title)
    TextView mScreenTitle;
    @BindView(R.id.tv_catalog)
    TextView mCatalog;
    @BindView(R.id.tv_contact)
    TextView mContact;

    @BindView(R.id.main_fragment_container)
    FrameLayout mFrameLayout;

    ActionBarDrawerToggle mToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        mToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        FragmentUtils.replaceFragment(this, R.id.main_fragment_container, CategoriesFragment.getInstance(), false);
    }

    @Override
    public void onBackStackChanged() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            mToolbar.setNavigationOnClickListener(null);
            mToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            mDrawer.addDrawerListener(mToggle);
            mToggle.syncState();
            mToggle.setDrawerIndicatorEnabled(true);
        } else {
            mToggle.setDrawerIndicatorEnabled(false);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    Fragment currFragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment_container);
                    trans.remove(currFragment);
                    trans.commit();
                    manager.popBackStack();
                }
            });
        }
    }

    @OnClick(R.id.tv_catalog)
    public void openCatalog(){
        FragmentUtils.replaceFragment(this, R.id.main_fragment_container, CategoriesFragment.getInstance(), false);
        mDrawer.closeDrawers();
    }

    @OnClick(R.id.tv_contact)
    public void openContact(){
        FragmentUtils.replaceFragment(this, R.id.main_fragment_container, ContactFragment.getInstance(), false);
        mDrawer.closeDrawers();
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    protected void createPresenter() {

    }

    public void setScreenTitle(String title){
        mScreenTitle.setText(title);
    }

}
