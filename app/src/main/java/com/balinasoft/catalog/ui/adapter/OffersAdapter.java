package com.balinasoft.catalog.ui.adapter;

import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balinasoft.catalog.R;
import com.balinasoft.catalog.database.model.Offer;
import com.balinasoft.catalog.ui.fragments.OfferDetailsFragment;
import com.balinasoft.catalog.util.FragmentUtils;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kirill on 16.1.17.
 */
public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.OffersHolder> {

    private static final int LAYOUT = R.layout.item_offer;

    private List<Offer> mOffers;
    private WeakReference<FragmentActivity> mFragmentActivity;

    public OffersAdapter(FragmentActivity activity, List<Offer> offers) {
        mFragmentActivity = new WeakReference<>(activity);
        mOffers = offers;
    }

    @Override
    public OffersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mFragmentActivity.get());
        View view = inflater.inflate(LAYOUT, parent, false);
        return new OffersHolder(view);
    }

    @Override
    public void onBindViewHolder(OffersHolder holder, int position) {
        final Offer offer = mOffers.get(position);
        holder.bindHolder(offer);
        holder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentUtils.replaceFragment(mFragmentActivity.get(), R.id.main_fragment_container,
                        OfferDetailsFragment.getInstance(offer), true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mOffers.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class OffersHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.offer_cover)
        ImageView mOfferCover;
        @BindView(R.id.offer_name)
        TextView mOfferName;
        @BindView(R.id.offer_weight)
        TextView mOfferWeight;
        @BindView(R.id.offer_price)
        TextView mOfferPrice;
        @BindView(R.id.item_offer)
        LinearLayout mLinearLayout;

        public OffersHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindHolder(Offer offer) {
            setCover(offer.getPicture());
            mOfferName.setText(offer.getName());
            mOfferWeight.setText(mFragmentActivity.get().getResources().getString(R.string.offer_weight) + offer.getWeight());
            mOfferPrice.setText(mFragmentActivity.get().getResources().getString(R.string.offer_price) + offer.getPrice());
        }

        private void setCover(String picture) {
            if (picture == null) {
                picture = "http://ufa.farfor.ru/media/menu/products/7.jpg";
            }
            Uri uri = Uri.parse(picture);
            Picasso.with(mFragmentActivity.get())
                    .load(uri)
                    .fit()
                    .centerCrop()
                    .into(mOfferCover);
        }
    }
}
