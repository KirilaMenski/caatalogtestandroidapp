package com.balinasoft.catalog.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balinasoft.catalog.R;
import com.balinasoft.catalog.database.model.Category;
import com.balinasoft.catalog.database.model.Offer;
import com.balinasoft.catalog.ui.activities.MainActivity;
import com.balinasoft.catalog.ui.adapter.OffersAdapter;
import com.balinasoft.catalog.ui.base.BaseFragment;
import com.balinasoft.catalog.ui.base.BasePresenter;
import com.balinasoft.catalog.ui.presenter.OffersFragmentPresenter;
import com.balinasoft.catalog.ui.view.OffersFragmentView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kirill on 16.1.17.
 */

public class OffersFragment extends BaseFragment implements OffersFragmentView {

    private static final int LAYOUT = R.layout.fragment_offers;
    private static final String EXTRA_CATEGORY = "com.balinasoft.catalog.ui.fragments.category";

    private OffersFragmentPresenter mPresenter;
    private OffersAdapter mAdapter;

    @BindView(R.id.offer_list_recycler)
    RecyclerView mOfferRec;

    public static OffersFragment getInstance(Category category) {
        OffersFragment fragment = new OffersFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(LAYOUT, container, false);
        ButterKnife.bind(this, view);
        Category category = (Category) getArguments().getSerializable(EXTRA_CATEGORY);
        mPresenter.initialize(category);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.loadOffers();
    }

    @Override
    public BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void createPresenter() {
        mPresenter = new OffersFragmentPresenter(this);
    }

    @Override
    public void setAdapter(List<Offer> offers) {
        mAdapter = new OffersAdapter(getActivity(), offers);
        mOfferRec.setLayoutManager(new LinearLayoutManager(getContext()));
        mOfferRec.setAdapter(mAdapter);
    }

    @Override
    public void setScreenTitle(String title) {
        ((MainActivity) getActivity()).setScreenTitle(title);
    }
}
