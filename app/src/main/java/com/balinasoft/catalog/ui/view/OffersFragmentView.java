package com.balinasoft.catalog.ui.view;

import com.balinasoft.catalog.database.model.Offer;
import com.balinasoft.catalog.ui.base.BaseContextView;

import java.util.List;

/**
 * Created by kirill on 16.1.17.
 */
public interface OffersFragmentView extends BaseContextView {

    void setAdapter(List<Offer> offers);

    void setScreenTitle(String title);

}
