package com.balinasoft.catalog.util;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by kirill on 13.12.16.
 */

public class NetworkUtils {

    public static boolean isNetworkConnected(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

}
