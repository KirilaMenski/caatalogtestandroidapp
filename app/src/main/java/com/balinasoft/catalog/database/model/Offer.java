package com.balinasoft.catalog.database.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by kirill on 16.1.17.
 */

@DatabaseTable(tableName = "offer")
public class Offer implements Serializable {

    @DatabaseField(columnName = "id")
    private int mId;
    @DatabaseField(generatedId = true, columnName = "offer_id")
    private int mOfferId;
    @DatabaseField(columnName = "name")
    private String mName;
    @DatabaseField(columnName = "url")
    private String mUrl;
    @DatabaseField(columnName = "price")
    private double mPrice;
    @DatabaseField(columnName = "description")
    private String mDescription;
    @DatabaseField(columnName = "picture")
    private String mPicture;
    @DatabaseField(columnName = "category_id", foreign = true, foreignAutoRefresh = true,
            columnDefinition = "integer references category(id) on delete cascade")
    private Category mCategory;
    @DatabaseField(columnName = "weight")
    private double mWeight;
    @DatabaseField(columnDefinition = "calorific")
    private double mCalorificVal;
    @DatabaseField(columnDefinition = "protein")
    private String mProtein;
    @DatabaseField(columnDefinition = "fat")
    private String mFat;
    @DatabaseField(columnDefinition = "carbohydrate")
    private String mCarbohydrate;

    public Offer() {

    }

    public Offer(int id, int offerId, String carbohydrate, String name, double price, String description,
                 String picture, Category category, double weight, double calorificVal,
                 String protein, String fat, String url) {
        mId = id;
        mOfferId = offerId;
        mCarbohydrate = carbohydrate;
        mName = name;
        mPrice = price;
        mDescription = description;
        mPicture = picture;
        mCategory = category;
        mWeight = weight;
        mCalorificVal = calorificVal;
        mProtein = protein;
        mFat = fat;
        mUrl = url;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getPicture() {
        return mPicture;
    }

    public void setPicture(String picture) {
        mPicture = picture;
    }

    public Category getCategory() {
        return mCategory;
    }

    public void setCategory(Category category) {
        mCategory = category;
    }

    public double getWeight() {
        return mWeight;
    }

    public void setWeight(double weight) {
        mWeight = weight;
    }

    public double getCalorificVal() {
        return mCalorificVal;
    }

    public void setCalorificVal(double calorificVal) {
        mCalorificVal = calorificVal;
    }

    public String getProtein() {
        return mProtein;
    }

    public void setProtein(String protein) {
        mProtein = protein;
    }

    public String getFat() {
        return mFat;
    }

    public void setFat(String fat) {
        mFat = fat;
    }

    public String getCarbohydrate() {
        return mCarbohydrate;
    }

    public void setCarbohydrate(String carbohydrate) {
        mCarbohydrate = carbohydrate;
    }

    public String getUrl() {
        return mUrl;
    }

    public int getOfferId() {
        return mOfferId;
    }

    public void setOfferId(int offerId) {
        mOfferId = offerId;
    }

    public void setUrl(String url) {
        mUrl = url;
    }
}
