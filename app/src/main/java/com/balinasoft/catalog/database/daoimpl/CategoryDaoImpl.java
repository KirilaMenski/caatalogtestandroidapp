package com.balinasoft.catalog.database.daoimpl;

import android.content.Context;

import com.balinasoft.catalog.CatalogApp;
import com.balinasoft.catalog.database.DataBaseHelper;
import com.balinasoft.catalog.database.dao.CategoryDao;
import com.balinasoft.catalog.database.model.Category;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.lang.ref.WeakReference;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kirill on 16.1.17.
 */

public class CategoryDaoImpl implements CategoryDao {

    private static CategoryDaoImpl mInstance;

    private WeakReference<Context> mContext;
    private DataBaseHelper mDataBaseHelper;
    private Dao<Category, Integer> mDao;

    public static synchronized CategoryDaoImpl getInstance() {
        if (mInstance == null) {
            mInstance = new CategoryDaoImpl();
        }
        return mInstance;
    }

    private CategoryDaoImpl() {
        mContext = new WeakReference<>(CatalogApp.getAppContext());
        mDataBaseHelper = OpenHelperManager.getHelper(mContext.get(), DataBaseHelper.class);
        mDao = mDataBaseHelper.getCategoryDao();
    }

    @Override
    public void addCategory(Category category) {
        try {
            mDao.create(category);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteCategory(Category category) {
        try {
            mDao.delete(category);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void editCategory(Category category) {
        try {
            mDao.update(category);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Category getCategoryById(int id) {
        try {
            QueryBuilder<Category, Integer> queryBuilder = mDao.queryBuilder();
            queryBuilder.where().eq("category_id", id);
            List<Category> accountList = queryBuilder.query();
            if (accountList.size() > 0)
                return accountList.get(0);
            else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Category> getCategories() {
        List<Category> categories = new ArrayList<>();
        try {
            categories = mDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }
}
