package com.balinasoft.catalog.database.dao;

import com.balinasoft.catalog.database.model.Category;

import java.util.List;

/**
 * Created by kirill on 16.1.17.
 */

public interface CategoryDao {

    void addCategory(Category category);

    void deleteCategory(Category category);

    void editCategory(Category category);

    Category getCategoryById(int id);

    List<Category> getCategories();

}
