package com.balinasoft.catalog.database.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kirill on 16.1.17.
 */

@DatabaseTable(tableName = "category")
public class Category implements Serializable {

    @DatabaseField(generatedId = true, columnName = "id")
    private int mId;
    @DatabaseField(columnName = "category_id")
    private int mCategoryId;
    @DatabaseField(columnName = "name")
    private String mName;
    @ForeignCollectionField
    private ForeignCollection<Offer> mOffers;

    public Category() {

    }

    public Category(int id, String name) {
        mId = id;
        mName = name;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String category) {
        mName = category;
    }

    public List<Offer> getOffers() {
        List<Offer> offers = new ArrayList<>();
        if (mOffers == null) {
            return offers;
        }
        for (Offer offer : mOffers) {
            offers.add(offer);
        }
        return offers;
    }

    public int getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(int categoryId) {
        mCategoryId = categoryId;
    }
}
