package com.balinasoft.catalog.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.balinasoft.catalog.database.model.Category;
import com.balinasoft.catalog.database.model.Offer;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by kirill on 16.1.17.
 */

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "Catalog.db";
    private static final int DATABASE_VERSION = 8;

    private Dao<Category, Integer> mCategoryDao = null;
    private Dao<Offer, Integer> mOfferDao = null;

    public DataBaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, Offer.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Category.class, false);
            TableUtils.dropTable(connectionSource, Offer.class, false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<Category, Integer> getCategoryDao(){
        if(mCategoryDao == null){
            try {
                mCategoryDao = getDao(Category.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mCategoryDao;
    }

    public Dao<Offer, Integer> getOfferDao(){
        if(mOfferDao == null){
            try {
                mOfferDao = getDao(Offer.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mOfferDao;
    }

}
