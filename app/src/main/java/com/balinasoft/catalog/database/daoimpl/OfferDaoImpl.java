package com.balinasoft.catalog.database.daoimpl;

import android.content.Context;

import com.balinasoft.catalog.CatalogApp;
import com.balinasoft.catalog.database.DataBaseHelper;
import com.balinasoft.catalog.database.dao.OfferDao;
import com.balinasoft.catalog.database.model.Offer;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.lang.ref.WeakReference;
import java.sql.SQLException;

/**
 * Created by kirill on 16.1.17.
 */

public class OfferDaoImpl implements OfferDao {

    private static OfferDaoImpl mInstance;

    private WeakReference<Context> mContext;
    private DataBaseHelper mDataBaseHelper;
    private Dao<Offer, Integer> mDao;

    public static synchronized OfferDaoImpl getInstance(){
        if(mInstance == null){
            mInstance = new OfferDaoImpl();
        }
        return mInstance;
    }

    private OfferDaoImpl(){
        mContext = new WeakReference<>(CatalogApp.getAppContext());
        mDataBaseHelper = OpenHelperManager.getHelper(mContext.get(), DataBaseHelper.class);
        mDao = mDataBaseHelper.getOfferDao();
    }

    @Override
    public void addOffer(Offer offer) {
        try {
            mDao.create(offer);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteOffer(Offer offer) {
        try {
            mDao.delete(offer);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void editOffer(Offer offer) {
        try {
            mDao.update(offer);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
