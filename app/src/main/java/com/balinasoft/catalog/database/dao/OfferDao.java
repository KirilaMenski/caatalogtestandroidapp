package com.balinasoft.catalog.database.dao;

import com.balinasoft.catalog.database.model.Offer;

/**
 * Created by kirill on 16.1.17.
 */

public interface OfferDao {

    void addOffer(Offer offer);

    void deleteOffer(Offer offer);

    void editOffer(Offer offer);

}
