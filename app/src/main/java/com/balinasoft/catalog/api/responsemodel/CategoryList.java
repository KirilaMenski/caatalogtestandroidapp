package com.balinasoft.catalog.api.responsemodel;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

/**
 * Created by kirill on 17.1.17.
 */
@Element(name = "category")
public class CategoryList {

    @Attribute(name = "id", required = false)
    private int mId;
    @Element(name = "category", required = false)
    private String mCategory;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }
}
