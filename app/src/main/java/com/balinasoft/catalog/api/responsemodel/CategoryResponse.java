package com.balinasoft.catalog.api.responsemodel;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kirill on 17.1.17.
 */

@Element(name = "categories")
public class CategoryResponse implements Serializable {

    @ElementList(entry = "category", required = false, inline = true)
    List<CategoryList> mCategoryLists;

    public List<CategoryList> getCategoryLists() {
        return mCategoryLists;
    }

    public void setCategoryLists(List<CategoryList> categoryLists) {
        mCategoryLists = categoryLists;
    }
}
