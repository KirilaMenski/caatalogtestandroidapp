package com.balinasoft.catalog.api;

import com.balinasoft.catalog.api.responsemodel.Catalog;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by kirill on 17.1.17.
 */

public interface ApiInterface {

    @GET("getyml/?key=ukAXxeJYZN")
    Observable<Catalog> loadData();

}
