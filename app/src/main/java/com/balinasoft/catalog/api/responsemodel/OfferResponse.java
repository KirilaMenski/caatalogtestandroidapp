package com.balinasoft.catalog.api.responsemodel;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kirill on 17.1.17.
 */

@Element(name = "offer")
public class OfferResponse implements Serializable {

    @Attribute(name = "id", required = false)
    private int mId;
    @Element(name = "url", required = false)
    private String mUrl;
    @Element(name = "name", required = false)
    private String mName;
    @Element(name = "price", required = false)
    private double mPrice;
    @Element(name = "description", required = false)
    private String mDescription;
    @Element(name = "picture", required = false)
    private String mPicture;
    //    @DatabaseField(columnName = "category_id", foreign = true, foreignAutoRefresh = true,
//            columnDefinition = "integer references category(id) on delete cascade")
//    private Category mCategory;
    @Element(name = "categoryId")
    private int mCategoryId;
    @ElementList(entry = "param", required = false, inline = true)
    List<OfferParam> mOfferParams;
//    @Element(name = "Вес", required = false)
//    private double mWeight;
////    @Element(name = "Каллорийность", required = false)
//    private double mCalorificVal;
//    @Element(name = "Белки", required = false)
//    private String mProtein;
//    @Element(name = "Жиры", required = false)
//    private String mFat;
//    @Element(name = "Углеводы", required = false)
//    private String mCarbohydrate;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getPicture() {
        return mPicture;
    }

    public void setPicture(String picture) {
        mPicture = picture;
    }

//    public Category getCategory() {
//        return mCategory;
//    }
//
//    public void setCategory(Category category) {
//        mCategory = category;
//    }

//    public double getWeight() {
//        return mWeight;
//    }
//
//    public void setWeight(double weight) {
//        mWeight = weight;
//    }
//
//    public double getCalorificVal() {
//        return mCalorificVal;
//    }
//
//    public void setCalorificVal(double calorificVal) {
//        mCalorificVal = calorificVal;
//    }
//
//    public String getProtein() {
//        return mProtein;
//    }
//
//    public void setProtein(String protein) {
//        mProtein = protein;
//    }
//
//    public String getFat() {
//        return mFat;
//    }
//
//    public void setFat(String fat) {
//        mFat = fat;
//    }
//
//    public String getCarbohydrate() {
//        return mCarbohydrate;
//    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

//    public void setCarbohydrate(String carbohydrate) {
//        mCarbohydrate = carbohydrate;
//    }

    public int getCategoryId() {
        return mCategoryId;
    }

    public List<OfferParam> getOfferParams() {
        return mOfferParams;
    }

    public void setOfferParams(List<OfferParam> offerParams) {
        mOfferParams = offerParams;
    }

    public void setCategoryId(int categoryId) {
        mCategoryId = categoryId;

    }
}
