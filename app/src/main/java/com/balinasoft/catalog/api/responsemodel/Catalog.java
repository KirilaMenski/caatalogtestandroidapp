package com.balinasoft.catalog.api.responsemodel;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by kirill on 17.1.17.
 */

@Root(name = "yml_catalog")
public class Catalog {

    @Attribute(name = "date")
    private String mDate;
    @Element(name = "shop")
    private Shop mShop;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Shop getShop() {
        return mShop;
    }

    public void setShop(Shop shop) {
        mShop = shop;
    }
}
