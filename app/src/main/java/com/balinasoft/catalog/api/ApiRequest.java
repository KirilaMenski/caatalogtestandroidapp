package com.balinasoft.catalog.api;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by kirila on 16.1.17.
 */
public class ApiRequest {

    static ApiRequest apiRequest;
    ApiInterface api;

    public static final String MAIN_URL = "http://ufa.farfor.ru/";

    public static ApiRequest getInstance() {
        if (apiRequest == null) {
            apiRequest = new ApiRequest();
        }
        return apiRequest;
    }

    private ApiRequest() {
        Retrofit retrofit = getRetrofit();
        api = retrofit.create(ApiInterface.class);
    }

    public Retrofit getRetrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder().addInterceptor(interceptor);
        client.readTimeout(5 * 60, TimeUnit.SECONDS);
        client.addInterceptor(new Interceptor() {
                                  @Override
                                  public okhttp3.Response intercept(Chain chain) throws IOException {
                                      Request original = chain.request();
                                      Request.Builder requestBuilder = original.newBuilder();
                                      requestBuilder.header("Content-Type", "application/json");
                                      Request request = requestBuilder.method(original.method(), original.body())
                                              .build();
                                      okhttp3.Response response = chain.proceed(request);
                                      return response;
                                  }
                              }
        );
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MAIN_URL)
                .client(client.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        return retrofit;
    }

    public ApiInterface getApi() {
        return api;
    }

}
