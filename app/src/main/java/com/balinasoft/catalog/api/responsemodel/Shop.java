package com.balinasoft.catalog.api.responsemodel;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by kirill on 17.1.17.
 */

@Root(name = "shop")
public class Shop {

    @Element(name = "categories", required = false)
    CategoryResponse mCategoryResponses;

    @Element(name = "offers", required = false)
    OfferList mOffer;

    public CategoryResponse getCategoryResponses() {
        return mCategoryResponses;
    }

    public void setCategoryResponses(CategoryResponse categoryResponses) {
        mCategoryResponses = categoryResponses;
    }

    public OfferList getOffer() {
        return mOffer;
    }

    public void setOffer(OfferList offer) {
        mOffer = offer;
    }
}
