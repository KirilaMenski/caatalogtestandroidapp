package com.balinasoft.catalog.api.responsemodel;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Text;

/**
 * Created by kirill on 17.1.17.
 */
@Element(name = "param")
public class OfferParam {

    @Attribute(name = "name", required = false)
    private String mName;
    @Path("param")
    @Text(required = false)
    private String mParam;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getParam() {
        return mParam;
    }

    public void setParam(String param) {
        mParam = param;
    }
}
