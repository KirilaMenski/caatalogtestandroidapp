package com.balinasoft.catalog.api.responsemodel;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kirill on 17.1.17.
 */

@Element(name = "offers")
public class OfferList implements Serializable {

    @ElementList(entry = "offer", required = false, inline = true)
    private List<OfferResponse> mOffers;

    public List<OfferResponse> getOffers() {
        return mOffers;
    }

    public void setOffers(List<OfferResponse> offers) {
        mOffers = offers;
    }
}
