package com.balinasoft.catalog;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

/**
 * Created by kirill on 16.1.17.
 */

public class CatalogApp extends MultiDexApplication {

    private static Context sContext;

    public void onCreate() {
        super.onCreate();
        sContext = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static Context getAppContext() {
        return sContext;
    }

}
